require('dotenv').config()

module.exports = {
  port: process.env.PORT,
  token: {
    privateKey: process.env.RSA_PRIVATE_KEY_FILE,
    publicKey: process.env.RSA_PUBLIC_KEY_FILE,
    expiresIn: process.env.TOKEN_EXPIRES_IN,
    issuer: process.env.TOKEN_ISSUER,
    audience: process.env.TOKEN_AUDIENCE,
  },
  db: {
    conectionString: process.env.DB_AUTH_CONECTION_STRING,
  }
}