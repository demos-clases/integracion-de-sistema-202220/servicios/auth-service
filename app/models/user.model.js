const mongoose = require('mongoose');
const crypto = require('crypto');

const { Schema } = mongoose;

const UserSchema = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  personId: { type: String, required: true },
  //hash : String, 
  salt : String 
}, 
{ timestamps: true }
);
// Method to set salt and hash the password for a user 
UserSchema.methods.setPassword = function(password) { 
  // Creating a unique salt for a particular user 
  this.salt = crypto.randomBytes(16).toString('hex'); 
  // Hashing user's salt and password with 1000 iterations, 
  this.hash = crypto.pbkdf2Sync(password, this.salt,  
  1000, 64, `sha512`).toString(`hex`); 
 }; 
   
 // Method to check the entered password is correct or not 
 UserSchema.methods.validPassword = function(password) { 
    const hash = crypto.pbkdf2Sync(password,  
    this.salt, 1000, 64, `sha512`).toString(`hex`); 
    return this.password === hash; 
 }; 
// Compile model from schema
var UserModel = mongoose.model('User', UserSchema );

module.exports = UserModel;
