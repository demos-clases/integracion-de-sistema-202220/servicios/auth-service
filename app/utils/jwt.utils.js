const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('./../../config');


//generate RSA https://cryptotools.net/rsagen
exports.generateToken = (user) => {
  // sign with RSA SHA256
  const privateKey = fs.readFileSync(config.token.privateKey).toString();
  console.log('pk', privateKey);
  const payload = {
    user: user,
    audience: config.token.audience,
    issuer: config.token.issuer,
  };
  const properties = {
    algorithm: 'RS256',
    expiresIn: config.token.expiresIn,
  };
  console.log('pre sign');
  const token = jwt.sign(payload, privateKey, properties);
  return token;
};

exports.validateToken = (token) => {
  const publicKey = fs.readFileSync(config.token.publicKey).toString();
  console.log('pub', publicKey);
  const properties = {algorithms: ['RS256']};
  const decoded = jwt.verify(token, publicKey, properties);
  return decoded;
};
