const jwtUtils = require('../utils/jwt.utils');
const UserModel = require('../models/user.model');
const crypto = require('crypto');

exports.login = async (req, res) => {
  const {email, password} = req.body;
  if (email && password) {
    // validamos en la BD
    const user = await UserModel.findOne({email}).exec();
    if (user !== null && user.validPassword(password)) {
      // si es correcto, generamos el token
      const token = jwtUtils.generateToken({
        id: user.id, email: 
        user.email, personId: 
        user.personId});
      res.status(200).json({ 
        jwt: token, 
        user: {
          email: user.email, 
          personId: user.personId} 
      });
    } 
    else { 
      return res.status(400).json({message : "Usuario o clave incorrectas."}); 
    } 
  } else {
    res.status(500).json({
      status: 'Error',
      message: 'Correo o clave invalidos'
    });
  }

};

exports.register = async (req, res) => {
  // Creating empty user object 
  let user = req.body;

  const salt = crypto.randomBytes(16).toString('hex'); 
  // Hashing user's salt and password with 1000 iterations, 
  const hash = crypto.pbkdf2Sync(user.password, salt,  
      1000, 64, `sha512`).toString(`hex`); 
  user.password = hash;
  user.salt = salt;
  try {
    const newUser = await UserModel.create(user);
    res.status(201).json({id: newUser.id, email: newUser.email, personId: newUser.personId});
  } catch (error) {
    res.status(500).json({error: error.message});
  }

}

exports.verify = (req, res) => {
  const {token} = req.body;
  try {
    const decoded = jwtUtils.validateToken(token);
    res.status(200).json(decoded);
  } catch(err) {
    console.error(err);
    res.status(403).json({
      error: 'No authorizado',
      message: 'token invalido'
    });
  }
};