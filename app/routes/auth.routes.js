const express = require('express');
const AuthCtrl = require('./../controllers/auth.controller');

const router = express.Router();


router.post('/register', AuthCtrl.register);
router.post('/login', AuthCtrl.login);
router.post('/verify', AuthCtrl.verify);
router.patch('/refresh', () => {});
router.delete('/cancel', () => {});

module.exports = router;