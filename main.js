const express = require('express');
const config = require('./config');
//const authMdw = require('./app/middlewares/jwt.middleware');
const mongoose = require('mongoose');
const authRoutes = require('./app/routes/auth.routes');

mongoose.connect(config.db.conectionString, { useNewUrlParser: true });

const app = new express();
app.use(express.json());
app.use('/api/v1/auth', authRoutes);
app.listen(config.port, function() {
  console.info(`Servidor en express iniciado en el puerto ${config.port}`);
});