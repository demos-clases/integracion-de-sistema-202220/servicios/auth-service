FROM node:lts-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
COPY ssh-keys/rsa.key /rsa.key
COPY ssh-keys/rsa.pub /rsa.pub
RUN chmod +r /rsa.key
RUN chmod +r /rsa.pub
RUN npm install --production --silent && mv node_modules ../
COPY . .
RUN chown -R node /usr/src/app
USER node
CMD ["npm", "start"]
